import logo from "./logo.svg";
import "./App.css";
import DemoJSS from "./JSS_StyledComponent/DemoJSS/DemoJSS";
import DemoThemes from "./JSS_StyledComponent/Themes/DemoThemes";
import TodoList from "./JSS_StyledComponent/BaiTapStyleComponent/TodoList/TodoList";
import LifeCycleReact from "./LifeCycleReact/LifeCycleReact";

function App() {
  return (
    <div>
      {/* <DemoJSS /> */}
      {/* <DemoThemes /> */}
      <TodoList />
      {/* <LifeCycleReact /> */}
    </div>
  );
}

export default App;
