import React, { Component } from "react";
import { Button, SmallButton } from "../Components/Button";
import { StyledLink } from "../Components/Link";
import { TextField } from "../Components/TextField";

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button bgPrimary fontSize2x>
          Hello Hà
        </Button>
        <SmallButton>Hello Hà</SmallButton>
        <StyledLink>Thiên Hà</StyledLink>
        <TextField inputColor="green" />
      </div>
    );
  }
}
