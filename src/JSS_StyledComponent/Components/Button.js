import styled from "styled-components";

export const Button = styled.button`
  background: ${(props) => (props.bgPrimary ? `blue` : "orange")};
  color: #fff;
  border: none;
  border-radius: 0.5rem;
  font-weight: bold;
  padding: 1rem;
  font-size: ${(props) => (props.fontSize2x ? `2rem` : "1rem")};
  opacity: 1;
  &:hover {
    opacity: 0.7;
    trasition: all o.5s;
  }
`;
export const SmallButton = styled(Button)`
  background-color: red;
  font-size: 3rem;
  padding: 4rem;
`;
