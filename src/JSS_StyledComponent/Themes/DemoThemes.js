import styled, { ThemeProvider } from "styled-components";
import React, { Component } from "react";
const configDarkTheme = {
  background: "#666",
  color: "#FFF",
  fontSize: "15px",
  fontWeight: "300",
};
const configLightTheme = {
  background: "#6633FF",
  color: "black",
  fontSize: "20px",
  fontWeight: "300",
};
export default class DemoThemes extends Component {
  state = {
    currentTheme: configDarkTheme,
  };
  handleChangeTheme = (event) => {
    this.setState({
      currentTheme:
        event.target.value == "1" ? configDarkTheme : configLightTheme,
    });
  };

  render() {
    const DivStyle = styled.div`
      color: ${(props) => props.theme.color};
      padding: 5%;
      background-color: ${(props) => props.theme.background};
      font-size: ${(props) => props.theme.fontSize};
      font-weight: ${(props) => props.theme.fontWeight};
    `;
    return (
      <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>Hello Thien Ha</DivStyle>
        <select onChange={this.handleChangeTheme}>
          <option value="1" key="">
            DarkTheme
          </option>
          <option value="2" key="">
            LightTheme
          </option>
        </select>
      </ThemeProvider>
    );
  }
}

// export const DemoThemes = (propsComponent) => {
//   state = {
//     currentTheme: configDarkTheme,
//   };
//   handleChangeTheme = (event) => {
//     this.setState({
//       currentTheme:
//         event.target.value == "1" ? configDarkTheme : configLightTheme,
//     });
//   };
//   const configDarkTheme = {
//     background: "#666",
//     color: "#FFF",
//     fontSize: "15px",
//     fontWeight: "300",
//   };
//   const configLightTheme = {
//     background: "#6633FF",
//     color: "black",
//     fontSize: "20px",
//     fontWeight: "300",
//   };
//   const DivStyle = styled.div`
//     color: ${(props) => props.theme.color};
//     padding: 5%;
//     background-color: ${(props) => props.theme.background};
//     font-size: ${(props) => props.theme.fontSize};
//     font-weight: ${(props) => props.theme.fontWeight};
//   `;
//   return (
//     <ThemeProvider theme={this.state.currentTheme}>
//       <DivStyle>Hello Thien Ha</DivStyle>
//       <select onChange={this.handleChangeTheme}>
//         <option value="1" key="">
//           DarkTheme
//         </option>
//         <option value="2" key="">
//           LightTheme
//         </option>
//       </select>
//     </ThemeProvider>
//   );
// };
